$(function () {
    
   /*
    * Function to auto populate the row letter for the seating
    * based on ul data-row attribute
    */
   $('ul.seating').each(function (i) {
       
      //Get the row from the data attribute for the ul
       var row = $(this).data('row');
       /*
        * Locate the first LI item and convert it to rownum class 
        * and add the row letter 
        */
       
       $(this).find("li").eq(0).addClass('rownum').text(row);
       
   });
   
    
   $('ul.seating li').on('click',function (){
       var seatsSelected  = recalcSeating(); 
       var maxSeats = 3;
       
       //We can not overwrite a reserved class, an aisle or letter item
       if ($(this).hasClass('reserved') || $(this).hasClass('aisle') || $(this).hasClass('rownum')) return;
       
       //Are we unchecking an existing selection
       if ($(this).hasClass('chosen')) {
               
               $(this).removeClass('chosen');
               seatsSelected = recalcSeating();
               
               return; //Must return otherwise it will fall through
               
        }
       
       //Determine how many seats are currently selected
       //If maximum seats chosen, determine if we are unselecting a seat
       if (seatsSelected >= maxSeats) {
          
           return;
       }
       
       $(this).addClass('chosen');
        
      seatsSelected = recalcSeating();
       //console.log("Count is: " + seatsSelected);
       
   }); //ul.seating li
   
   function recalcSeating() {
       
       var seatsSelected = $('ul.seating li.chosen').length;
       
       $('#seatcount').text(seatsSelected);
       
       showSelectedSeats();
       
       return seatsSelected;
       
       
   }
   
   function showSelectedSeats() {
       
       var html = [];
       
       $('ul.seating li.chosen').each(function (index) {
           
           html.push('<span class="label label-info chosenSeats">');
           
           html.push($( this ).parent().data('row'));
           html.push($( this ).data('seat'));
           html.push('</span>');
           
       });
       
       $('#showSelectedSeats').empty().append(html.join(' '));   
       
       
   }
    
});